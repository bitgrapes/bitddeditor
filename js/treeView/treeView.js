(function($) {
//-----------------------------------------------------------
    
$.fn.bitDDTreeView = function() {
    var treeRoot = this;
    var treeController = {};
    var newPlaceholder = $('<li class="bitDD_dragging" id="treeViewPlaceholder"><span class="treeBranchLast"></span><div><span class="disclose"></span><span class="treeBranch"></span></div></li>');
    var placeholder = null;
    var indentSpace = parseInt(treeRoot.find('ol:first').css('padding-left'));
    
    function getRoot() {
        return treeRoot;
    }
    
    treeRoot.disableSelection();
    
    //-----------------------------------------------
    // Tree manipulation methods
    //-----------------------------------------------
    function addNode(wsParent, wsPrevSibling, wsNextSibling, wsEl, name, isPlaceholder) {
        
        if( placeholder == null) {
            placeholder = newPlaceholder;
        }        
        if(isPlaceholder == undefined) {
            isPlaceholder = false;
            placeholder.detach();
        }
        
        var parentNodeEl = null;
        var prevSiblingNodeEl = null;
        var nextSiblingNodeEl = null;
        
        if( wsEl.attr('id') && name == undefined)
            name = 'ElID: ' + wsEl.attr('id');
        else if( isPlaceholder )
            name = '';
        else if(name == undefined)
            name = 'Element';      
        
        if(wsParent) parentNodeEl = wsParent.data('refVtEl');
        if(wsPrevSibling) prevSiblingNodeEl = wsPrevSibling.data('refVtEl');
        if(wsNextSibling) nextSiblingNodeEl = wsNextSibling.data('refVtEl');
        
        var currentNodeEl;
        
        if( !isPlaceholder) {
            currentNodeEl = wsEl.data('refVtEl');        
            // if wsEl is a new Element, create new node, else get the current one
            if( !currentNodeEl ) {        
                currentNodeEl = $('<li><span class="treeBranchLast"></span><div><span class="disclose expanded"></span><span class="treeBranch"></span><span class="nodename">' + name + '</span></div><ol></ol></li>');
                wsEl.data('refVtEl', currentNodeEl);
            }
            else {
                // check if parent has other children and show/hide expand icon accordingly
                var parent = currentNodeEl.parents('li:first');

                currentNodeEl.detach();
                //console.log(parent);
                if( parent.children('ol:first').children().length == 0 ) {
                    parent.find('.disclose:first').css('visibility', 'hidden');
                    var aaa = parent.find('.disclose:first');
                }
                else
                    parent.find('.disclose:first').css('visibility', 'visible');                        
            }
        }
        else {
            currentNodeEl = placeholder;
        }
        
        
        if( currentNodeEl.parents('li:first').find('ol li').length == 1)
            currentNodeEl.parents('li:first').find('.disclose:first').css('visibility', 'hidden');
        
        if( prevSiblingNodeEl ) {
            prevSiblingNodeEl.after(currentNodeEl);
        }
        else if( nextSiblingNodeEl ) {
            nextSiblingNodeEl.before(currentNodeEl);
        }
        else if( parentNodeEl ) {
            parentNodeEl.children('ol:first').append(currentNodeEl);
        }
        else {
            treeRoot.children('ol:first').append(currentNodeEl);
        }
        
            
        // check if the node has children and show/hide expand icon accordingly
        if( currentNodeEl.children('ol:first').children().length == 0 )
            currentNodeEl.find('.disclose:first').css('visibility', 'hidden');
        else
            currentNodeEl.find('.disclose:first').css('visibility', 'visible');
        
        if(currentNodeEl.parents('li:first'))
            currentNodeEl.parents('li:first').find('.disclose:first').css('visibility', 'visible');
                    
        
        // save reference to element in workspace
        if( !isPlaceholder )
            currentNodeEl.data('refWsEl', wsEl);
        
     
        treeController.addDraggableBehavior(currentNodeEl);
        
        return currentNodeEl;
    }
        
    function removeNode(wsEl) {
        var vtEl = wsEl.data('refVtEl');
        if( vtEl )
            vtEl.remove();
    }
    
    
    function getWsElement(nodeEl) {
        return nodeEl.data('refWsEl');
    }
    
    function selectTreeElement(vtEl) {
        vtEl.addClass('selected');
    }
    
    function deselectTreeElement(vtEl) {
        vtEl.removeClass('selected');
    }
    
    //----------------------------------------------
    // Generate tree from existing document
    //----------------------------------------------
    function loadTree(rootEl) {
        if( !rootEl.hasClass('bitDD_ws_root') ) 
            return;
        
        treeRoot.html('<div><span class="disclose expanded"></span>Root</div><ol></ol>');
        treeRoot.data('refWsEl', rootEl);
        
        createSubTree(rootEl);  
        
        initEvents();
    }
    
    function createSubTree(wsParentNode) {        
        var componentChildren = wsParentNode.children('.bitDD_ws_component');
       
        componentChildren.each(function(index, item) {
            var newNode = addNode(wsParentNode, null, null, $(item)); 
            
            createSubTree( $(item) );
            //if( newNode.children('ol:first').children().length == 0 )
            //    newNode.find('.disclose:first').css('visibility', 'hidden');
                
        });
    }
    
    //----------------------------------------------
    
    
    //----------------------------------------------
    // Event handlers
    //----------------------------------------------
    
    function initEvents() {
       
        treeRoot.on('click', '.disclose', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var el = $(e.target);
            
            el.toggleClass('expanded').toggleClass('closed');
            
            if( el.hasClass('expanded') )
                el.parent().parent().find('ol:first').show();
            else
                el.parent().parent().find('ol:first').hide();
        });
        
       
        treeRoot.on('click', 'li > div', function(e) {       
            e.stopPropagation();
            e.preventDefault();            
            $(e.target).parent().data('refWsEl').click();            
        });
    }
    
    
    var nodelist = null;
    
    function startDrag(wsEl) {
        if(wsEl != undefined) {
            var nodeEl = wsEl.data('refVtEl');
            placeholder = nodeEl;
        }
        else {
            placeholder = newPlaceholder;
        }
        placeholder.addClass('bitDD_dragging');
    }
    
    function drag(helper) {
        
        if(nodelist == null) {
            var nodelist = treeRoot.parent().find('li:not(.bitDD_dragging):not(.bitDD_dragging li):visible');
            
        }
        
        var xa = parseInt( helper.offset().left );
        var ya = parseInt( helper.offset().top );
        //var xb = parseInt( helper.outerWidth() + xa );
        var yb = parseInt( helper.outerHeight() + ya );
        
        var closestnode = null;
        var lastRelpos = 0; // after
        nodelist.each( function(index, item) {
            if(lastRelpos == 1) return;
            var it = $(item);
            //var x0 = parseInt( it.offset().left );
            var y0 = parseInt( it.offset().top );
            //var x1 = parseInt( it.outerWidth() + xa );
            var y1 = parseInt( it.outerHeight() + ya );
            
            //var hintersect = Math.min(y1,yb) - Math.max(y0, ya);
            //console.log(ya, y0);
            if( ya > y0 && lastRelpos == 0 ) {
                lastRelpos == 1;
                closestnode = it;
            }
        } );
        
        treeRoot.parent().find('li.closest').removeClass('closest');
		var oldParent = placeholder.parents('li:first');
		
        if(closestnode) {
            var finalParent = null;
            var finalPrev = null;
            var finalNext = null;
            
            closestnode.addClass('closest');
            var closestIsRoot = false;
            if(closestnode.get(0) == treeRoot.get(0))
                closestIsRoot = true;
            
            // check the depth where we want to insert the element
            var x0 = parseInt( closestnode.offset().left );
            var dx = (xa - x0);
                        
            if(closestIsRoot || ( dx > indentSpace && closestnode.data('refWsEl').hasClass('droppable') ) ) {   // child
                //console.log('child');
                closestnode.find('ol:first').prepend(placeholder);      
            }
            else if ( dx < -indentSpace) {  // ancestor
                // console.log('ancestor');
                var ancestorStep = Math.floor( (-dx) / indentSpace );                
                var whereToInsert = closestnode;//.parents('li:first');
                
                while(ancestorStep > 0) {                    
                    whereToInsert = whereToInsert.parents('li:first');
                    ancestorStep--;
                    
                    if( whereToInsert.get(0) == treeRoot.get(0) ) {
                        whereToInsert = closestnode;
                        break;
                    }
                }
                
                /*if( whereToInsert.get(0) == treeRoot.get(0) ) {
                    console.log('ASD');
                    whereToInsert = closestnode;
                }*/
                placeholder.insertAfter(whereToInsert);      
                
                console.log( placeholder.prev() );
            }  
            else {  // same depth
                //console.log('same');
                placeholder.insertAfter(closestnode);
            }
         
			finalNodeParent = placeholder.parents('li:first');
			finalNodeParent.find('div:first .disclose').css('visibility', 'visible');
            finalParent = finalNodeParent.data('refWsEl');
            finalPrev = placeholder.prevAll('li:first').data('refWsEl');
            finalNext = placeholder.nextAll('li:first').data('refWsEl');
            
            movingElement( finalParent, finalPrev, finalNext );
        }
        else {
            placeholder.detach();            
        }
		
		if( oldParent.find('ol:first li').length == 0 )
			oldParent.find('div:first > .disclose').css('visibility', 'hidden');
    }
    
    
    function stopDrag () {
        placeholder.removeClass('bitDD_dragging');
    }
    
    function dragOut( helper ) {
		if(placeholder) {
			var placeholderParent = placeholder.parents('li:first');
			if( placeholderParent.find('ol:first li').length == 1)
				placeholderParent.find('.disclose:first').css('visibility', 'hidden');
			placeholder.detach();
		}
        treeRoot.parent().find('li.closest').removeClass('closest');
    }
    
    //----------------------------------------------
    // Setup Interactions with WS
    //----------------------------------------------
    treeController.addDraggableBehavior = function(elem) {}
    
    function setAddDraggableBehaviorFunction(func) {
        treeController.addDraggableBehavior = func;
    }
    
    
    //----------------------------------------------
    
    var movingElement = function() {};
    
    // set references to methods
    treeController.getRoot = getRoot;
    treeController.addNode = addNode;
    treeController.removeNode = removeNode;
    
    treeController.loadTree = loadTree;
    treeController.setAddDraggableBehaviorFunction = setAddDraggableBehaviorFunction;
    
    // events
    treeController.selectNode = selectTreeElement;
    treeController.deselectNode = deselectTreeElement;
    treeController.startDrag = startDrag;
    treeController.drag = drag;
    treeController.stopDrag = stopDrag;
    treeController.dragOut = dragOut;
    
    treeController.setMovingElementCallback = function(cb) {
        movingElement = cb;
    };
    
    return treeController;
}

//-----------------------------------------------------------
})(jqbitDD);