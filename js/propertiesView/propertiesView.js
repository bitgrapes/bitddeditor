(function($) {
//-----------------------------------------------------------
    
$.fn.bitDDPropertiesView = function() {
    
    var propertyTypes = {};

    var stopClass = ['ui-draggable', 'ui-draggable-handle'];
    
    
    var propertiesTab = $(this);
    var selectedElement = null;
        
    function selectElement(el) {
        selectedElement = el;
    }
    
    function showProperties(el) {
        selectedElement = el;
        
        var componentName = el.data('bitDD_component_name');
        propertiesTab.parent().find('.bitDD_properties_componentName').text(componentName);
        
        propertiesTab.find('input[name=element_id]').val(el.attr('id'));
        propertiesTab.find('input[name=element_name]').val(el.attr('name'));
        
        var classes = el.attr('class').split(' ');        
        var baseClasses = el.data('bitDD_baseClasses').split(' ');
        var showClasses = classes.filter(function(val) { 
            return (baseClasses.indexOf(val) == -1) && (stopClass.indexOf(val) == -1 );
        });
        
        propertiesTab.find('input[name=element_class]').val( showClasses.join(' ') );
        
        // manage custom properties
        var component = componentLibrary[componentName];
        
        var customProps = propertiesTab.find('ul.bitDD_custom_properties:first');
        //customProps.empty();
        customProps.children().detach();
        if( component.properties ) {
            var propManager = propertiesController.propertyManager;
            for( var k in component.properties ) {
                if(propManager[component.properties[k]]) {
                    var manager = propManager[component.properties[k]];
                    var propHtml = manager.propertiesViewRow(k, el);
                    var row = $('<li><div class="bitDD_label_wrapper"><label>' + k + '</label></div><div class="bitDD_input_wrapper"></div></li>');
                    row.find('.bitDD_input_wrapper').append(propHtml);
                    customProps.append(row);
                }
                /*else {
                    var row = $('<li><div class="bitDD_label_wrapper"><label>' + k + '</label></div><div class="bitDD_input_wrapper"><button class="' + component.properties[k] + '">Edit...</button></div></li>');
                    customProps.append(row);
                }*/
            }
        }
        
        //propertiesTab.find('ul.bitDD_custom_properties:first').html( customProps );
    }
        
    
    window.bitDD_showEditWindow = function(content) {
        $('#bitDD_property_edit_window').children().detach();
        $('#bitDD_property_edit_window').append(content);
        $('#bitDD_property_edit_window_container').show();
        $('#bitDD_property_edit_window').show();
        var w = $('#bitDD_property_edit_window').outerWidth();
        var h = $('#bitDD_property_edit_window').outerHeight();
        
        var vpw = $('body').width();
        var vph = $('body').height();
        
        var x = ( vpw - w ) / 2;
        var y = ( vph - h ) / 2;
        
        $('#bitDD_property_edit_window').css('top', y+'px');
        $('#bitDD_property_edit_window').css('left', x+'px');
    };
    
    window.bitDD_closeEditWindow = function() {
        $('#bitDD_property_edit_window').hide();
        $('#bitDD_property_edit_window_container').hide();
    };
    
    
    // Setup events
    propertiesTab.on('change', 'input', function(e) {
        var el = $(e.target);
        var name = el.attr('name');
        
        var prop = name.split('_');
        
        if(prop.length == 2) {  // attribute            
            
            if( prop[1] == 'id' ) {
                var node = selectedElement.data('refVtEl');
                node.find('span.nodename:first').text( el.val() );
                selectedElement.attr(prop[1], el.val() );
            }
            else if( prop[1] == 'class' ) {
                var baseClasses = selectedElement.data('bitDD_baseClasses').split(' ');
                var classes = el.val().trim() + ' ' + baseClasses.join(' ') + ' ' + stopClass.join(' ');
                selectedElement.attr(prop[1], classes );
            }
        }
    });
    
    
    // Expose methods
    var propertiesController = {};    
    propertiesController.propertyManager = {};
    
    propertiesController.selectElement = selectElement;
    propertiesController.showProperties = showProperties;
                
    return propertiesController;
}

//-----------------------------------------------------------
}(jqbitDD))