if( !window.bitDD_propertyTypesManager ) bitDD_propertyTypesManager = {};

jqbitDD(document).ready(function() {
    var treeController = jqbitDD('#bitDD_workspace_tree_root').bitDDTreeView();
    var propertiesController = jqbitDD('#bitDD_element_properties').bitDDPropertiesView();
    
    propertiesController.propertyManager = bitDD_propertyTypesManager;
    
    
    jqbitDD('#bitDD_workspace').bitDDWorkspaceView(treeController, propertiesController);
            
    var $ = jqbitDD;
    
    var wsPlus = $('<div>QWE</div>');
    //treeController.addNode(null, $('#bitDD_workspace_tree_root ol li:first'), wsPlus, 'test 1');
    //treeController.addNode(wsPlus, null, $('<div id="test2"></div>'));
    
    var ws_scale = 1;
    var matrixStr = $('#bitDD_workspace').css('transform');
    var matrix = matrixStr.match(/-?[\d\.]+/g);
    //console.log(matrix);
    if( matrix && matrix[0] == matrix[3] )
        ws_scale = matrix[0];
    
    treeController.loadTree(jqbitDD('#bitDD_workspace_main_container'));
    
   
    //$('#mystyle').text('');
    var source = $('#bitDD_gloabl_style');
    var ss = source.text();
    
    var parser = new CSSParser();
    parser.loadStyle(ss);
});