componentLibrary = {
    'component_1' : {
        name: 'component_1',
        icon: '<div class="draggable">Component 1</div>',
        template: '<div id="component_1" class="component_1" data-property="Text">Component 1</div>',
        properties: {
            'Text': 'text'
        }
    },
    'component_2' : {
        name: 'component_2',
        icon: '<div class="draggable">Component 2</div>',
        template: '<div id="component_2" class="component_2 droppable"><div style="width: 30%; margin: 5px; border: solid 1px red;" class="bitDD_droparea"></div> <div style="width: 30%; margin: 5px; border: solid 1px red;" class="bitDD_droparea"></div> </div>'
    },
	'component_3' : {
        name: 'component_3',
        icon: '<div class="draggable">Component 3</div>',
        template: '<div id="component_3" class="component_3 droppable">CCC</div>'
    },
    'heading_1': {
        name: 'Heading',
        icon: '<div class="draggable">Heading</div>',
        template: '<div class="heading_1 droppable"></div>'
    },
    'fullWidthContainer': {
        name: 'Full width container',
        icon: '<div class="draggable">Full Width</div>',
        template: '<div class="fullwidth_container droppable"></div>'
    },
    'limitedWidthContainer': {
        name: 'Limited width container',
        icon: '<div class="draggable">Limited Width</div>',
        template: '<div class="limitedwidth_container droppable"></div>'
    },
    'textelement': {
        name: 'Text Element',
        icon: '<div class="draggable">Text</div>',
        template: '<div class="textelement" data-bitDD-Text>Custom text</div>',
        properties: {
            'Text': 'text'
        }
    }
};

// Load components library
(function($) {
$(document).ready(function() {
//-----------------------------------------------------------        
    
    console.log('Loading components library');
    
    var components_container = $('#bitDD_component_library .content_container:first');
    components_container.html('');
    
    for(k in componentLibrary) {
        var c = componentLibrary[k];
        var el = $(c.icon).addClass('bitDD_component_icon').addClass('draggable').attr('name', k);
        components_container.append(el);
    }
    
//-----------------------------------------------------------
});
} (jqbitDD));