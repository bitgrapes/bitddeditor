jqbitDD(document).ready(function() {
    
    jqbitDD('#bitDD_workspace').bitDDWorkspaceView();
    
    var $ = jqbitDD;
    var ws_scale = 1;
    var matrixStr = $('#bitDD_workspace').css('transform');
    var matrix = matrixStr.match(/-?[\d\.]+/g);
    //console.log(matrix);
    if( matrix && matrix[0] == matrix[3] )
        ws_scale = matrix[0];
    
    //-----------------------------------------------------------
    // TREE VIEW INITIALIZATION
    //-----------------------------------------------------------
    $('.sortable').nestedSortable({
		forcePlaceholderSize: true,
        handle: 'div',
		helper:	'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div',
		maxLevels: 3,
		isTree: true,
		expandOnHover: 700,
		startCollapsed: true,
        protectRoot: true    
    });
    
    $('#bitDD_workspace_tree').on('click', 'span.disclose', function(e) {
        $(e.target).parents('li:first').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
    });
        
    //-----------------------------------------------------------
    
    // TODO: move to jquery plugin
    
    var droppableList = $('#bitDD_workspace .droppable');
    var movingElement;
    var helperElement = $('<div id="bitDD_helper_element"></div>');
    //$('#bitDD_workspace').append(helperElement);
    var selectorElement = $('<div id="bitDD_selector_element"></div>');
    
    var selectedElement = null;
    
    $('#bitDD_workspace').append(selectorElement);
    
    var deadzone = 0;
    var lastX = null;
    var lastY = null;
    var lastClosestEl = null;
    var lastClosestType = 0;  // 0: parent, 1: 
    var deadZoneTreshold = 20;
        
    $('.draggable').draggable({
        /*helper: 'clone',*/
        connectWith: '.sortable',
        helper: dragHelperCreator,
        opacity: '0.7',
        zIndex: 99999999999,
        cursorAt: { left: 10, top: 10 },
        start: dragStartHandler,
        drag: dragDragHandler,
        stop: dragStopHandler,
        /*start_: function(e, ui) {
            
            lastX = e.pageX;
            lastY = e.pageY;
                                
            droppableList = $( $('#bitDD_workspace .droppable').get().reverse() );
            
            droppableList.each(function(index, item) {
                var it = $(item);
                it.data('treedepth', it.parents().length);
            });
            
           droppableList.sort(function(a, b) {
                return $(b).data('treedepth') - $(a).data('treedepth');
            } ); 
            
            droppableList.sort(function(a, b) {    
                return $(b).zIndex() - $(a).zIndex();
            } ); 
            
            if( $(e.target).parents('#bitDD_workspace').length > 0 )
                movingElement = $(e.target).css('opacity', 0.5);            
            else
                movingElement = $(e.target).clone(false).css('opacity', 0.5);
        },
        drag_: function(e, ui) {
            
            var mx = e.pageX;
            var my = e.pageY;
                    
            if( deadzone > 0 ) {                
                deadzone -= Math.abs(mx - lastX );
                deadzone -= Math.abs(my - lastY );
                
                lastX = mx;
                lastY = my;
                return;                
            }
            lastX = mx;
            lastY = my;
            
            var el = $(ui.helper);
            
            var x0 = parseInt( el.offset().left );
            var y0 = parseInt( el.offset().top );
            var x1 = parseInt( el.outerWidth() + x0 );
            var y1 = parseInt( el.outerHeight() + y0 );
            
            var helperArea = (x1 - x0) * (y1 - y0);
          
                        
            // filter out elements with intersection area < 40% of the maximum
            var validDroppables = droppableList.filter( function(index, element) {
                var el = $(element);
                el.data('iareaPerc', 0 );
                var xa = parseInt( el.offset().left );
                var ya = parseInt( el.offset().top );
                var xb = parseInt( el.outerWidth() + xa );
                var yb = parseInt( el.outerHeight() + ya );
                                
                if( xb < x0 || xa > x1 || yb < y0 || ya > y1)
                    return false;
                
                //var iarea = intersectArea(x0, y0, x1, y1, xa, xb, ya, yb);
                //var carea = (xb - xa) * (yb - ya);
         
                var iareaPerc = intersectAreaPerc(x0, y0, x1, y1, xa, ya, xb, yb);
                if( iareaPerc < 0.4 ) {                    
                    return false;
                }
                
                el.data('iareaPerc', iareaPerc );
                
                return true;
            } );
            
            droppableList.removeClass('hovering');
            
            if(validDroppables.length > 0) {
                
                var currentContainer = $(validDroppables[0]);
                var currentContainerParent = currentContainer.parent();
                
                var bestContainer = currentContainer;
                
                // find the best element at the same level of the current one
                validDroppables.each(function(index, item) {
                    if(index == 0) return;
                    var it = $(item);
                    if( it.parent().get(0) == currentContainerParent.get(0)
                      && it.data('iarea') > currentContainer.data('iarea')) {
                        currentContainer = it;
                        currentContainerParent = currentContainer.parent();
                    }
                });
                
                
                var closestSibling = null;
                var prevArea = 0;
                // directions: 0 = after, 1 = before 
                var prevDir = 0;
                var relDir = -1;
                
                
                if( currentContainerParent.hasClass('droppable') ) {
                    var nextContainer = currentContainer.next('.droppable');
                    var prevContainer = currentContainer.prev('.droppable');
                    if(nextContainer.get(0) == movingElement.get(0)) nextContainer = nextContainer.next('.droppable');
                    if(prevContainer.get(0) == movingElement.get(0)) prevContainer = prevContainer.prev('.droppable');
                    
                    // Check if user is trying to put new element between two existing elements
                    if( Math.abs( currentContainer.data('iareaPerc') - nextContainer.data('iareaPerc') ) < 0.4 ) {
                        //console.log('using parent ('+currentContainer.data('iareaPerc')+') ('+nextContainer.data('iareaPerc')+')');    
                        closestSibling = currentContainer;
                        relDir = 0;
                        currentContainer = currentContainer.parent(); 
                    }
                    else if( Math.abs( currentContainer.data('iareaPerc') - prevContainer.data('iareaPerc') ) < 0.4 ) {
                        closestSibling = currentContainer;
                        relDir = 1;
                        currentContainer = currentContainer.parent();
                    }
                    // check if user is movin on the edge of an existing element inside a droppable
                    else {
                        relDir = getRelativePositioningEl( $(ui.helper) , currentContainer );
                        if( relDir != -1 ) {
                            closestSibling = currentContainer;
                            currentContainer = currentContainer.parent(); 
                        }
                    }
                }
                
                                
                currentContainer.addClass('hovering');
                //console.log(validDroppables);
                               
                if( closestSibling != null) {
                    if(relDir == 1)
                        movingElement.insertBefore(closestSibling);
                    else
                        movingElement.insertAfter(closestSibling);
                    
                    if( lastClosestEl != closestSibling.get(0) || lastClosestType != 1 ) {
                        deadzone = deadZoneTreshold;                               
                    }
                    lastClosestEl = closestSibling.get(0);
                    lastClosestType = 1;
                }
                else {
                    closestSibling = null;
                    var futureSiblings = currentContainer.children();
                    futureSiblings.each(function(index, item) {
                        var el = $(item);
                        if(item == movingElement.get(0)) {                        
                            return;
                        }
                        
                        var relPos = getRelativePositioningEl( $(ui.helper) , el );
                        if( relPos == -1 ) relPos = 1;
                        if( prevDir == 0 && relPos == 1 ) {
                            prevDir = 1;
                            closestSibling = el;
                        }                                 
             
                    });
                    
                    if( closestSibling != null) {
                        movingElement.insertBefore(closestSibling);
                        if( lastClosestEl != closestSibling.get(0) || lastClosestType != 1 ) {
                            deadzone = deadZoneTreshold;  
                        }
                        lastClosestEl = closestSibling.get(0);
                        lastClosestType = 1;
                    }
                    else {
                        currentContainer.append(movingElement);
                        if( lastClosestEl != currentContainer.get(0) || lastClosestType != 0 ) {
                            deadzone = deadZoneTreshold;   
                        }
                        lastClosestEl = currentContainer.get(0);
                        lastClosestType = 0;
                    }
                }
                           
            
            }            
            else            
                movingElement.detach();        
        },
        stop_: function(e, ui) {
            droppableList.removeClass('hovering');
            movingElement.css('opacity', '');
        }*/
    });
    
    
    //-----------------------------------------------------------
    // DRAG AND DROP EVENT HANDLERS
    //-----------------------------------------------------------
    /*
     *
     */
    function dragStartHandler(e, ui) {            
        lastX = e.pageX;
        lastY = e.pageY;
        deselectElement();                        
        droppableList = $( $('#bitDD_workspace .droppable').get().reverse() );
            
        droppableList.each(function(index, item) {
            var it = $(item);
            it.data('treedepth', it.parents().length);
        });
            
        droppableList.sort(function(a, b) {
            return $(b).data('treedepth') - $(a).data('treedepth');
        } ); 
            
        droppableList.sort(function(a, b) {    
            return $(b).zIndex() - $(a).zIndex();
        } ); 
            
        if( $(e.target).parents('#bitDD_workspace').length > 0 )
            movingElement = $(e.target).css('opacity', 0.5);            
        else
            movingElement = $(e.target).clone(false).css('opacity', 0.5);
    }
        
    /*
     * 
     */
    function dragDragHandler(e, ui) {
            
        var mx = e.pageX;
        var my = e.pageY;
                    
        if( deadzone > 0 ) {                
            deadzone -= Math.abs(mx - lastX );
            deadzone -= Math.abs(my - lastY );
                
            lastX = mx;
            lastY = my;
            return;                
        }
        lastX = mx;
        lastY = my;
            
        var el = $(ui.helper);
            
        var x0 = parseInt( el.offset().left );
        var y0 = parseInt( el.offset().top );
        var x1 = parseInt( el.outerWidth() + x0 );
        var y1 = parseInt( el.outerHeight() + y0 );
            
        var helperArea = (x1 - x0) * (y1 - y0);
          
                        
        // filter out elements with intersection area < 40% of the maximum
        var validDroppables = droppableList.filter( function(index, element) {                        
            var el = $(element);
            
            if(element == movingElement.get(0) ) return false;
            if( movingElement.find(el).length > 0) return false;
            
            el.data('iareaPerc', 0 );
            var xa = parseInt( el.offset().left );
            var ya = parseInt( el.offset().top );
            var xb = parseInt( el.outerWidth() + xa );
            var yb = parseInt( el.outerHeight() + ya );
                                
            if( xb < x0 || xa > x1 || yb < y0 || ya > y1)
                return false;
                
            //var iarea = intersectArea(x0, y0, x1, y1, xa, xb, ya, yb);
            //var carea = (xb - xa) * (yb - ya);
         
            var iareaPerc = intersectAreaPerc(x0, y0, x1, y1, xa, ya, xb, yb);
            if( iareaPerc < 0.4 ) {                    
                return false;
            }
                
            el.data('iareaPerc', iareaPerc );
                
            return true;
        } );
            
        droppableList.removeClass('hovering');
            
        if(validDroppables.length > 0) {
                
            var currentContainer = $(validDroppables[0]);
            var currentContainerParent = currentContainer.parent();
                
            var bestContainer = currentContainer;
                
            // find the best element at the same level of the current one
            validDroppables.each(function(index, item) {
                if(index == 0) return;
                var it = $(item);
                if( it.parent().get(0) == currentContainerParent.get(0)
                      && it.data('iarea') > currentContainer.data('iarea')) {
                    currentContainer = it;
                    currentContainerParent = currentContainer.parent();
                }
            });
                
                
                
            /*
             * Check if user is moving over multiple element with same edge
             */
            /*if( currentContainerParent.hasClass('droppable') && currentContainer.data('iareaPerc') == currentContainerParent.data('iareaPerc') ) {
                var n_over = 0;
                var ccp = currentContainerParent;
                var cc = currentContainer;
                while( ccp != null && ccp.hasClass('droppable') && cc.data('iareaPerc') == ccp.data('iareaPerc') ) {
                    n_over++;
                    cc = ccp;
                    ccp = ccp.parent();
                } 
                    
                var step = 0.2 / n_over;
                var n_sel = n_over - Math.floor( (currentContainer.data('iareaPerc') - 0.4) / step );
                    
                for(var i = 0 ; i < n_sel ; i++) {
                    currentContainer = currentContainerParent;
                    currentContainerParent = currentContainer.parent();
                }
            }*/
                
                
                
            var closestSibling = null;
            var prevArea = 0;
            /* directions: 0 = after, 1 = before */
            var prevDir = 0;
            var relDir = -1;
                
                
            if( currentContainerParent.hasClass('droppable') ) {
                var nextContainer = currentContainer.next('.droppable');
                var prevContainer = currentContainer.prev('.droppable');
                if(nextContainer.get(0) == movingElement.get(0)) nextContainer = nextContainer.next('.droppable');
                if(prevContainer.get(0) == movingElement.get(0)) prevContainer = prevContainer.prev('.droppable');
                    
                // Check if user is trying to put new element between two existing elements
                if( Math.abs( currentContainer.data('iareaPerc') - nextContainer.data('iareaPerc') ) < 0.4 ) {
                    //console.log('using parent ('+currentContainer.data('iareaPerc')+') ('+nextContainer.data('iareaPerc')+')');    
                    closestSibling = currentContainer;
                    relDir = 0;
                    currentContainer = currentContainer.parent(); 
                }
                else if( Math.abs( currentContainer.data('iareaPerc') - prevContainer.data('iareaPerc') ) < 0.4 ) {
                    closestSibling = currentContainer;
                    relDir = 1;
                    currentContainer = currentContainer.parent();
                }
                // check if user is movin on the edge of an existing element inside a droppable
                else {
                    relDir = getRelativePositioningEl( $(ui.helper) , currentContainer );
                    if( relDir != -1 ) {
                        closestSibling = currentContainer;
                        currentContainer = currentContainer.parent(); 
                    }
                }
            }
                
                                
            currentContainer.addClass('hovering');
            //console.log(validDroppables);
                               
            if( closestSibling != null) {
                if(relDir == 1)
                    movingElement.insertBefore(closestSibling);
                else
                    movingElement.insertAfter(closestSibling);
                
                if( lastClosestEl != closestSibling.get(0) || lastClosestType != 1 ) {
                    deadzone = deadZoneTreshold;                               
                }
                lastClosestEl = closestSibling.get(0);
                lastClosestType = 1;
            }
            else {
                closestSibling = null;
                var futureSiblings = currentContainer.children();
                futureSiblings.each(function(index, item) {
                    var el = $(item);
                    if(item == movingElement.get(0)) {                        
                        return;
                    }
                        
                    var relPos = getRelativePositioningEl( $(ui.helper) , el );
                    if( relPos == -1 ) relPos = 1;
                    if( prevDir == 0 && relPos == 1 ) {
                        prevDir = 1;
                        closestSibling = el;
                    }                                 
             
                });
                    
                if( closestSibling != null) {
                    movingElement.insertBefore(closestSibling);
                    if( lastClosestEl != closestSibling.get(0) || lastClosestType != 1 ) {
                        deadzone = deadZoneTreshold;  
                    }
                    lastClosestEl = closestSibling.get(0);
                    lastClosestType = 1;
                }
                else {
                    currentContainer.append(movingElement);
                    if( lastClosestEl != currentContainer.get(0) || lastClosestType != 0 ) {
                        deadzone = deadZoneTreshold;   
                    }
                    lastClosestEl = currentContainer.get(0);
                    lastClosestType = 0;
                }
            }
                           
        
        }            
        else            
            movingElement.detach();        
    }
    
    
    /*
     *
     */
    function dragStopHandler(e, ui) {
        droppableList.removeClass('hovering');
        movingElement.css('opacity', '');
        
        movingElement.draggable({            
            helper: dragHelperCreator,
            opacity: '0.7',
            zIndex: 99999999999,
            cursorAt: { left: 10, top: 10 },
            start: dragStartHandler,
            drag: dragDragHandler,
            stop: dragStopHandler
        });
        
        movingElement.click(clickSelectHandler);
    }
    
    
    function dragHelperCreator() {
        //var he = $('<div id="bitDD_helper_element"></div>');
        //$('#bitDD_workspace').append(he);
        $('#bitDD_workspace').append(helperElement);
        return helperElement;
        return he;
    }
    //-----------------------------------------------------------
    
    
    
    /*
     *
     */
    function clickSelectHandler(e) {
        var el = $(e.target);
        var ws = $('#bitDD_workspace');
        
        var off = ws.offset();
        
        var x = parseInt( el.offset().left - off.left );
        var y = parseInt( el.offset().top - off.top );
        var w = parseInt( el.outerWidth() );
        var h = parseInt( el.outerHeight() );
        
        console.log(x+','+y);
        
        selectorElement.css({ left: x, top: y, width: w, height: h });
        selectedElement = el;
        selectorElement.show();
    }
    
    function deselectElement() {
        if(selectedElement == null) return;
        selectorElement.hide();        
        selectedElement = null;
    }
    
    
    
    //$('body').append('<style>div { padding: 20px; }</style>');
    console.log( $('#mystyle').text() );
    $('#mystyle').text('');
});


function intersectArea(x0, y0, x1, y1, xa, xb, ya, yb) {
    var px0 = Math.max(x0, xa);
    var px1 = Math.min(x1, xb);
    var py0 = Math.max(y0, ya);
    var py1 = Math.min(y1, yb);
            
    var dx = px1 - px0;
    var dy = py1 - py0;
                
    var iarea = dx * dy;
    
    return iarea;
}

/*
 * Return ratio (actual intersection area) / (maximum intersection area possible) of two rectangles
 */
function intersectAreaPerc(x0, y0, x1, y1, xa, ya, xb, yb) {
    // intersection points
    var px0 = Math.max(x0, xa);
    var px1 = Math.min(x1, xb);
    var py0 = Math.max(y0, ya);
    var py1 = Math.min(y1, yb);
    
    var idx = px1 - px0;
    var idy = py1 - py0;
       
    // intersection area
    var iarea = idx * idy;
    
    var pdx = Math.min(x1 - x0, xb - xa);
    var pdy = Math.min(y1 - y0, yb - ya);
    
    return iarea / (pdx * pdy);
}

/* return relative positioning Pab to Pxy: 0 = after, 1 = before */
function getRelativePositioning(x0, y0, x1, y1, xa, ya, xb, yb) {    
  
    var dxl = xa - x0; // if positive new element starts on the left
    var dxr = x1 - xb; // if positive new element end on the right
    var dyt = ya - y0; // if positive new element starts above the existing one
    var dyb = y1 - yb; // if positive new element starts below the existing one
    
    var relpos = 0;
    var horizontalRelPos = 0;
    var verticalRelPos = 0;
    if( dxl <= 0 && dxr <= 0 ) horizontalRelPos = -1;
    else if( dxl >= dxr ) horizontalRelPos = 1;
    
    if( dyt <= 0 && dyb <= 0 ) verticalRelPos = -1;
    else if( dyt >= dyb ) verticalRelPos = 1;
    
    
    //console.log('H: '+horizontalRelPos+', V: '+verticalRelPos);
    
    if( horizontalRelPos == -1 && verticalRelPos == -1) return 1;
    if( verticalRelPos == -1 ) return horizontalRelPos;
    /*if( horizontalRelPos == -1 )*/ return verticalRelPos;
    return 1;
}
    
/* return relative positioning el1 to el2: 0 = after, 1 = before, -1 unknown */
function getRelativePositioningEl(el1, el2) {
    var x0 = parseInt( el1.offset().left );
    var y0 = parseInt( el1.offset().top );
    var x1 = parseInt( el1.outerWidth() + x0 );
    var y1 = parseInt( el1.outerHeight() + y0 );
    
    var xa = parseInt( el2.offset().left );
    var ya = parseInt( el2.offset().top );
    var xb = parseInt( el2.outerWidth() + xa );
    var yb = parseInt( el2.outerHeight() + ya );
    
    var dxl = xa - x0; // if positive new element starts on the left
    var dxr = x1 - xb; // if positive new element end on the right
    var dyt = ya - y0; // if positive new element starts above the existing one
    var dyb = y1 - yb; // if positive new element starts below the existing one
    
    var relpos = 0;
    var horizontalRelPos = -1;
    var verticalRelPos = -1;
    
    if( dxl <= 0 && dxr <= 0 ) horizontalRelPos = -1;
    else if( dxl > 0 && dxr > 0 && ( Math.max(dxl, dxr) / Math.min(dxl, dxr) > 2 ) ) {
        dHalfLeft = dyl + (x1 - x0) / 2;
        dHalfRight = dyr + (x1 - x0) / 2;
        
        if( dHalfLeft / dHalfRight ) horizontalRelPos = 1;
        else if(dHalfRight / dHalfLeft ) horizontalRelPos = 0;
        
        //if( dxl > dxr ) horizontalRelPos = 1;
        //else if( dxl < dxr ) horizontalRelPos = 0;
    }
    else if( dxl > dxr && (x1 - x0) / dxl < 2 ) horizontalRelPos = 1;
    else if( dxr > dxl && (x1 - x0) / dxr < 2 ) horizontalRelPos = 0;
    
    if( dyt <= 0 && dyb <= 0 ) verticalRelPos = -1;
    else if( dyt > 0 && dyb > 0 && ( Math.max(dyt, dyb) / Math.min(dyt, dyb) > 2 ) ) {
        var dHalfTop = dyt + (y1 - y0) / 2;
        var dHalfBot = dyb + (y1 - y0) / 2;
          
        if( dHalfTop / dHalfBot > 1.6 ) verticalRelPos = 1;
        else if ( dHalfBot / dHalfTop > 1.6 ) verticalRelPos = 0;
        //if( dyt > dyb ) verticalRelPos = 1;
        //else if( dyb > dyt ) verticalRelPos = 0;
    }
    else if( dyt > dyb && (y1 - y0) / dyt < 2 ) verticalRelPos = 1;
    else if( dyb > dyt && (y1 - y0) / dyb < 2 ) verticalRelPos = 0;
    
    
    //console.log('H ('+dxl+','+dxr+'): '+horizontalRelPos+', V ('+dyt+','+dyb+'): '+verticalRelPos);
    
    if( horizontalRelPos == -1 && verticalRelPos == -1) return -1;
    if( verticalRelPos == -1 ) return horizontalRelPos;
    /*if( horizontalRelPos == -1 )*/ return verticalRelPos;
    return -1;
}