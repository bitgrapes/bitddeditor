(function($) {
//-----------------------------------------------------------
    
$.fn.bitDDWorkspaceView = function(tree, properties) {    
    var treeController = tree;
    var propertiesController = properties;
    var treeRoot = treeController.getRoot();
    //treeController.loadTree($(this));
    
    
    var wsRoot = $(this);
    
    var wsId = $(this).attr('id');
    //console.log(wsId);
    
    var droppableList = $('#bitDD_workspace .droppable');
    var movingElement;
    var helperElement = $('<div id="bitDD_helper_element"></div>');
    //$('#bitDD_workspace').append(helperElement);
    var selectorElement = $('<div id="bitDD_selector_element"></div>');
    
    var selectedElement = null;
    
    $('#bitDD_workspace').append(selectorElement);
    
    var deadzone = 0;
    var lastX = null;
    var lastY = null;
    var lastClosestEl = null;
    var lastClosestType = 0;  // 0: parent, 1: 
    var deadZoneTreshold = 16;
        
    $('.draggable').draggable({
        scroll:false,
        /*helper: 'clone',*/
        connectWith: '.sortable',
        helper: dragHelperCreator,
        opacity: '0.7',
        zIndex: 99999999999,
        cursorAt: { left: 10, top: 10 },
        start: dragStartHandler,
        drag: dragDragHandler,
        stop: dragStopHandler,       
        scroll: false
    });
    
    
    //-----------------------------------------------------------
    // DRAG AND DROP EVENT HANDLERS
    //-----------------------------------------------------------
    /*
     *
     */
    function dragStartHandler(e, ui) {            
        lastX = e.pageX;
        lastY = e.pageY;
        deselectElement();                        
        droppableList = $( $('#bitDD_workspace .droppable').get().reverse() );
            
        droppableList.each(function(index, item) {
            var it = $(item);
            it.data('treedepth', it.parents().length);
        });
            
        droppableList.sort(function(a, b) {
            return $(b).data('treedepth') - $(a).data('treedepth');
        } ); 
            
        droppableList.sort(function(a, b) {    
            return $(b).zIndex() - $(a).zIndex();
        } ); 
            
        // start dragging from Tree View
        if( $(e.target).parents('#bitDD_workspace_tree_container').length > 0 ) {
            movingElement = $(e.target).data('refWsEl').css('opacity', 0.5); 
            treeController.startDrag(movingElement);
        }
        // start dragging from Workspace View
        else if( $(e.target).parents('#bitDD_workspace').length > 0 ) {
            movingElement = $(e.target).css('opacity', 0.5);      
            treeController.startDrag(movingElement);
        }
        // start dragging from Components Library
        else {
            //movingElement = $(e.target).clone(false).css('opacity', 0.5);
            var name = $(e.target).attr('name');
            movingElement = $( componentLibrary[name].template ).addClass('bitDD_ws_component').css('opacity', 0.5);
            movingElement.data('bitDD_component_name', name);
            movingElement.data('bitDD_baseClasses', movingElement.attr('class'));
            treeController.startDrag();
        }
        
        var wsW = wsRoot.width();
        var wsH = wsRoot.height();
        
        //wsRoot.css('width', wsW+'px');
        //wsRoot.css('height', wsH+'px');
    }
        
    /*
     * 
     */
    function dragDragHandler(e, ui) {
            
        var mx = e.pageX;
        var my = e.pageY;
                    
        if( deadzone > 0 ) {                
            deadzone -= Math.abs(mx - lastX );
            deadzone -= Math.abs(my - lastY );
                
            lastX = mx;
            lastY = my;
            return;                
        }
        lastX = mx;
        lastY = my;
        
        var scrollY = wsRoot.scrollTop();
        var scrollX = wsRoot.scrollLeft();
        console.log('scrollY: '+scrollY);
        var el = $(ui.helper);
            
        var x0 = parseInt( el.offset().left );
        var y0 = parseInt( el.offset().top );
        var x1 = parseInt( el.outerWidth() + x0 );
        var y1 = parseInt( el.outerHeight() + y0 );
            
        var helperArea = (x1 - x0) * (y1 - y0);
          
        var wsx0 = parseInt( wsRoot.offset().left );
        var wsy0 = parseInt( wsRoot.offset().top );
        var wsx1 = parseInt( wsRoot.outerWidth() + wsx0 );
        var wsy1 = parseInt( wsRoot.outerHeight() + wsy0 );
             
        var wsiArea = intersectAreaPerc(x0, y0, x1, y1, wsx0, wsy0, wsx1, wsy1);
    
        
        if( wsiArea >= 0.4 ) {    // hovering workspace area
        
            // filter out elements with intersection area < 40% of the maximum
            var validDroppables = droppableList.filter( function(index, element) {                        
                var el = $(element);

                if(element == movingElement.get(0) ) return false;
                if( movingElement.find(el).length > 0) return false;

                el.data('iareaPerc', 0 );
                var xa = parseInt( el.offset().left );
                var ya = parseInt( el.offset().top );
                var xb = parseInt( el.outerWidth() + xa );
                var yb = parseInt( el.outerHeight() + ya );

                if( xb < x0 || xa > x1 || yb < y0 || ya > y1)
                    return false;

                //var iarea = intersectArea(x0, y0, x1, y1, xa, xb, ya, yb);
                //var carea = (xb - xa) * (yb - ya);

                var iareaPerc = intersectAreaPerc(x0, y0, x1, y1, xa, ya, xb, yb);
                if( iareaPerc < 0.4 ) {                    
                    return false;
                }

                el.data('iareaPerc', iareaPerc );

                return true;
            } );

            droppableList.removeClass('hovering');

            if(validDroppables.length > 0) {

                var currentContainer = $(validDroppables[0]);
                //var currentContainerParent = currentContainer.parent();
                var currentContainerParent = currentContainer.parents('.droppable:first');

                var bestContainer = currentContainer;

                // find the best element at the same level of the current one
                validDroppables.each(function(index, item) {
                    if(index == 0) return;
                    var it = $(item);
                    if( it.parent().get(0) == currentContainerParent.get(0)
                          && it.data('iareaPerc') > currentContainer.data('iareaPerc')) {
                        currentContainer = it;
                        currentContainerParent = currentContainer.parents('.droppable:first');
                    }
                });

                console.log( currentContainer );
                console.log('area first check '+currentContainer.data('iareaPerc'));


                /*
                 * Check if user is moving over multiple element with same edge
                 */
                /*if( currentContainerParent.hasClass('droppable') && currentContainer.data('iareaPerc') == currentContainerParent.data('iareaPerc') ) {
                    var n_over = 0;
                    var ccp = currentContainerParent;
                    var cc = currentContainer;
                    while( ccp != null && ccp.hasClass('droppable') && cc.data('iareaPerc') == ccp.data('iareaPerc') ) {
                        n_over++;
                        cc = ccp;
                        ccp = ccp.parent();
                    } 

                    var step = 0.2 / n_over;
                    var n_sel = n_over - Math.floor( (currentContainer.data('iareaPerc') - 0.4) / step );

                    for(var i = 0 ; i < n_sel ; i++) {
                        currentContainer = currentContainerParent;
                        currentContainerParent = currentContainer.parent();
                    }
                }*/



                var closestSibling = null;
                var prevArea = 0;
                /* directions: 0 = after, 1 = before */
                var prevDir = 0;
                var relDir = -1;
                var doNothing = false;  // flag: if true don't put the element on the WS (hovering over a non-droppable elem)
                
                
                if( currentContainerParent.hasClass('droppable') ) {
                    console.log('checking parentcontainer');
                    var nextContainer = currentContainer.next('.droppable');
                    var prevContainer = currentContainer.prev('.droppable');
                    if(nextContainer.get(0) == movingElement.get(0)) nextContainer = nextContainer.next('.droppable');
                    if(prevContainer.get(0) == movingElement.get(0)) prevContainer = prevContainer.prev('.droppable');

                    // Check if user is trying to put new element between two existing elements
                    if( Math.abs( currentContainer.data('iareaPerc') - nextContainer.data('iareaPerc') ) < 0.4 ) {
                        //console.log('using parent ('+currentContainer.data('iareaPerc')+') ('+nextContainer.data('iareaPerc')+')');    
                        closestSibling = currentContainer;
                        relDir = 0;
                        currentContainer = currentContainer.parent(); 
                    }
                    else if( Math.abs( currentContainer.data('iareaPerc') - prevContainer.data('iareaPerc') ) < 0.4 ) {
                        closestSibling = currentContainer;
                        relDir = 1;
                        currentContainer = currentContainer.parent();
                    }
                    // check if user is movin on the edge of an existing element inside a droppable
                    else if( currentContainer.data('iareaPerc') < 0.4 ) {
                        if( currentContainer != null)
                            relDir = getRelativePositioningEl( $(ui.helper) , currentContainer );                        
                        if( relDir != -1 ) {
                            closestSibling = currentContainer;
                            //currentContainer = currentContainer.parent(); 
                            currentContainer = currentContainerParent;
                            console.log('going to parent');
                        }
                        console.log('no droppable sibling found');
                    }
                }


                currentContainer.addClass('hovering');
                //console.log(validDroppables);
                
                
                console.log( currentContainer );
                console.log('area second check '+currentContainer.data('iareaPerc'));
                
                // Find the droppable area inside the current container
                //var areas = currentContainer.find('.bitDD_droparea:not(.droppable .bitDD_droparea)');
                var areas = currentContainer.find('.bitDD_droparea').not( function(index, item) {
                    return $(item).parents('.droppable:first').get(0) != currentContainer.get(0);
                } );
                var dropArea = null;
                
                console.log('---------------------');
                
                if( areas.length == 0 ) {       // put directly in container
                    dropArea = currentContainer;
                }
                else if( areas.length == 1 ) {
                    dropArea = $(areas.get(0));
                    if( intersectAreaPercElems(dropArea, $(ui.helper) ) < 0.4 )
                       doNothing = true;
                }
                else {                          // find the closest droparea
                    // TODO: gestione multi-droparea                    
                    var currentBestArea = 0;
                    areas.each(function(index, item) {
                        var i = $(item);
                        var a = intersectAreaPercElems( $(ui.helper), i );
                        if( a > 0.4 && a > currentBestArea ) {
                            currentBestArea = a;
                            dropArea = i;                            
                        }
                        console.log('area: '+a);
                    });
                    if(dropArea == null)
                        doNothing = true;
                }
                
                
                console.log( currentContainer );
                console.log('area third check ' + currentContainer.data('iareaPerc'));
                
				console.log('closestSibling: ' + closestSibling);
                if( closestSibling != null) {
                    if( !doNothing ) {
                        if(relDir == 1)
                            movingElement.insertBefore(closestSibling);
                        else
                            movingElement.insertAfter(closestSibling);

                        if( lastClosestEl != closestSibling.get(0) || lastClosestType != relDir ) {
                            deadzone = deadZoneTreshold;                               
                        }
                        lastClosestEl = closestSibling.get(0);
                        lastClosestType = relDir;
                    }
                    else {
                        movingElement.detach();    
                        treeController.dragOut( helperElement );
                        currentContainer.removeClass('hovering');
                    }
                }
                else {
                    closestSibling = null;
					
                    var futureSiblings;
					if( dropArea != null ) 
						//futureSiblings = dropArea.find('.bitDD_ws_component:not(.bitDD_ws_component .bitDD_ws_component)');
						futureSiblings = dropArea.children('.bitDD_ws_component');
					else
						futureSiblings = currentContainer.find('.bitDD_ws_component:not(.bitDD_ws_component .bitDD_ws_component)');
					
                    futureSiblings.each(function(index, item) {
                        var el = $(item);
                        if(item == movingElement.get(0)) {                        
                            return;
                        }

                        var relPos = getRelativePositioningEl( $(ui.helper) , el );
                        if( relPos == -1 ) relPos = 1;
                        if( prevDir == 0 && relPos == 1 ) {							
                            prevDir = 1;
                            closestSibling = el;
                            
                            if( intersectAreaPercElems($(ui.helper) , closestSibling) > 0.8 ) 
                                doNothing = true;
								
							console.log('closest sibling rel: ' + relPos + ', area: '+ intersectAreaPercElems($(ui.helper) , closestSibling) );
                        }   						
                    });
					if( closestSibling == null && futureSiblings.length > 0 ) {
						var lastEl = futureSiblings.last();
												
						if( lastEl.get(0) != movingElement.get(0) && intersectAreaPercElems( $(ui.helper) , lastEl ) > 0.8 )
							doNothing = true;
					}

                                        
                    if( !doNothing ) {
                        if( closestSibling != null) {
                            
                            console.log('insertbefore');
                            console.log(closestSibling);
                            movingElement.insertBefore(closestSibling);
                            if( lastClosestEl != closestSibling.get(0) || lastClosestType != 1 ) {
                                deadzone = deadZoneTreshold;  
                            }
                            lastClosestEl = closestSibling.get(0);
                            lastClosestType = 1;
                        }
                        else {
                            if( dropArea == null && areas.length == 0 )
                                currentContainer.append(movingElement);
                            else if( dropArea != null )
                                dropArea.append(movingElement);
                            else 
                                doNothing = true;

                            if( lastClosestEl != currentContainer.get(0) || lastClosestType != 0 ) {
                                deadzone = deadZoneTreshold;   
                            }
                            lastClosestEl = null; //currentContainer.get(0);
                            lastClosestType = 0;
                        }
                        
                        console.log( currentContainer );
                        console.log('area fourth check '+currentContainer.data('iareaPerc'));
                    }
                    else {
                        movingElement.detach();    
                        treeController.dragOut( helperElement );
                        currentContainer.removeClass('hovering');
                    }
                    
                }

                //console.log(lastClosestType);
                if( !doNothing ) {
                    treeController.addNode(currentContainer, (!lastClosestType ? $(lastClosestEl) : null), (lastClosestType ? $(lastClosestEl) : null), movingElement, null, true );
                }
				
            }            
            else {           
                movingElement.detach();    
                treeController.dragOut( helperElement );
            }

        }  // end drag over workspace area
        else {
            wsRoot.find('.hovering').removeClass('hovering');
            movingElement.detach();
            
            var treeContainer = $('#bitDD_workspace_tree_container');
            
            var tvx0 = parseInt( treeContainer.offset().left );
            var tvy0 = parseInt( treeContainer.offset().top );
            var tvx1 = parseInt( treeContainer.outerWidth() + tvx0 );
            var tvy1 = parseInt( treeContainer.outerHeight() + tvy0 );
             
            var tviArea = intersectAreaPerc(x0, y0, x1, y1, tvx0, tvy0, tvx1, tvy1);
            
            if( tviArea >= 0.8 ) {
                treeController.drag( helperElement );
            }
            else {
                treeController.dragOut( helperElement );                
            }
            
        }
        
        wsRoot.scrollTop(scrollY);
        wsRoot.scrollLeft(scrollX);
    }
    
    
    /*
     *
     */
    function dragStopHandler(e, ui) {
        if(movingElement.parent().length == 0) {
            treeController.removeNode(movingElement);
            movingElement.remove();
            return;
        }
        
        droppableList.removeClass('hovering');
        $('#bitDD_workspace .hovering').removeClass('hovering');
        movingElement.css('opacity', '');
        
        movingElement.draggable({            
            helper: dragHelperCreator,
            opacity: '0.7',
            zIndex: 99999999999,
            cursorAt: { left: 10, top: 10 },
            start: dragStartHandler,
            drag: dragDragHandler,
            stop: dragStopHandler,
            scroll: false
        });
        
        movingElement.click(clickSelectHandler);
        
        // TODO: remove pr and ne
        var pr = movingElement.prevAll('.bitDD_ws_component:first');
        var ne = movingElement.nextAll('.bitDD_ws_component:first');
        
        var nodeName = componentLibrary[movingElement.data('bitDD_component_name')].name;
        var treeNode = treeController.addNode(movingElement.parents('.droppable:first'), movingElement.prevAll('.bitDD_ws_component:first'), movingElement.nextAll('.bitDD_ws_component:first'), movingElement, nodeName);
     
        treeNode.draggable({            
            helper: dragHelperCreator,
            opacity: '0.7',
            zIndex: 99999999999,
            cursorAt: { left: 10, top: 10 },
            start: dragStartHandler,
            drag: dragDragHandler,
            stop: dragStopHandler,
            scroll:false
        });
        
        treeController.stopDrag();
    }
    
    function setDraggableBehavior(elem) {
        elem.draggable({            
            helper: dragHelperCreator,
            opacity: '0.7',
            zIndex: 99999999999,
            cursorAt: { left: 10, top: 10 },
            start: dragStartHandler,
            drag: dragDragHandler,
            stop: dragStopHandler,
            scroll:false
        });
    }
    treeController.setAddDraggableBehaviorFunction(setDraggableBehavior);
    
    
    function dragHelperCreator() {
        //var he = $('<div id="bitDD_helper_element"></div>');
        //$('#bitDD_workspace').append(he);
        //$('#bitDD_workspace').append(helperElement);
        $('body').append(helperElement);
        return helperElement;
        //return he;
    }
    //-----------------------------------------------------------
    
    function moveMovingElement(parent, prevSibling, nextSibling) {
        if(prevSibling != null) {
            movingElement.insertAfter( prevSibling );
            console.log('prev');
        }
        else if(nextSibling != null) {
            movingElement.insertBefore( nextSibling );
            console.log('next');
        }
        else if(parent != null) {
            var droparea  = parent.find('.bitDD_droparea:first');
            if(droparea.length == 0) droparea = parent;
            droparea.append( movingElement );
            console.log('parent');
        }
        
        wsRoot.find('.hovering').removeClass('hovering');
        movingElement.parents('.droppable:first').addClass('hovering');
    }
        
    treeController.setMovingElementCallback(moveMovingElement);
    
    
    /*
     *
     */
    function clickSelectHandler(e) {
        //deselectElement();
        e.stopPropagation();
        /*var el = $(this);
        var ws = $('#bitDD_workspace');
        
        var off = ws.offset();
        
        var x = parseInt( el.offset().left - off.left + ws.scrollLeft() );
        var y = parseInt( el.offset().top - off.top  + ws.scrollTop() );
        var w = parseInt( el.outerWidth() );
        var h = parseInt( el.outerHeight() );
        
        console.log(x+','+y);
        
        selectorElement.css({ left: x, top: y, width: w, height: h });
        selectedElement = el;
        selectorElement.show();
        
        treeController.selectNode(el.data('refVtEl'));*/
        selectElement( $(this) );
    }
    
    function selectElement(el) {
        deselectElement();
        
        //var el = $(this);
        var ws = $('#bitDD_workspace');
        
        var off = ws.offset();
        
        var x = parseInt( el.offset().left - off.left + ws.scrollLeft() );
        var y = parseInt( el.offset().top - off.top  + ws.scrollTop() );
        var w = parseInt( el.outerWidth() );
        var h = parseInt( el.outerHeight() );
        
        console.log(x+','+y);
        
        selectorElement.css({ left: x, top: y, width: w, height: h });
        selectedElement = el;
        selectorElement.show();
        
        treeController.selectNode(el.data('refVtEl'));
        
        propertiesController.showProperties(el);
    }
    
    function deselectElement() {
        if(selectedElement == null) return;
        selectorElement.hide();
        
        // get reference of prev selected node on the TreeView
        var refVtEl = selectedElement.data('refVtEl');
        
        selectedElement = null;               
        
        if(refVtEl)
            treeController.deselectNode(refVtEl);
    }
    
        
    //----------------------------------
    // load / save functions
    //----------------------------------
    
    function loadDocument() {
        var elems = $('#bitDD_workspace .bitDD_ws_component');
        elems.draggable({            
            helper: dragHelperCreator,
            opacity: '0.7',
            zIndex: 99999999999,
            cursorAt: { left: 10, top: 10 },
            start: dragStartHandler,
            drag: dragDragHandler,
            stop: dragStopHandler,
            scroll: false
        });
        
        elems.click(clickSelectHandler);
        
        
        treeController.loadTree($(this));
    }
    
    
    
    //----------------------------------    
    // init current workspace
    //----------------------------------
    wsRoot.find('.bitDD_ws_component').click(clickSelectHandler);
    wsRoot.click( function(e) {
        deselectElement();
    } );
    loadDocument();
    
}

//-----------------------------------------------------------


function intersectArea(x0, y0, x1, y1, xa, xb, ya, yb) {
    var px0 = Math.max(x0, xa);
    var px1 = Math.min(x1, xb);
    var py0 = Math.max(y0, ya);
    var py1 = Math.min(y1, yb);
            
    var dx = px1 - px0;
    var dy = py1 - py0;
                
    var iarea = dx * dy;
    
    return iarea;
}

/*
 * Return ratio (actual intersection area) / (maximum intersection area possible) of two rectangles
 */
function intersectAreaPerc(x0, y0, x1, y1, xa, ya, xb, yb) {
    // TODO quick check id empty intersection
    if( x0 > xb
        || x1 < xa
        || y0 > yb
        || y1 < ya )
        return 0;
    
    // intersection points
    var px0 = Math.max(x0, xa);
    var px1 = Math.min(x1, xb);
    var py0 = Math.max(y0, ya);
    var py1 = Math.min(y1, yb);
    
    var idx = px1 - px0;
    var idy = py1 - py0;
       
    // intersection area
    var iarea = idx * idy;
    
    var pdx = Math.min(x1 - x0, xb - xa);
    var pdy = Math.min(y1 - y0, yb - ya);
    
    return iarea / (pdx * pdy);
}
    
function intersectAreaPercElems(el1, el2) {
    var x0 = parseInt( el1.offset().left );
    var y0 = parseInt( el1.offset().top );
    var x1 = parseInt( el1.outerWidth() + x0 );
    var y1 = parseInt( el1.outerHeight() + y0 );
    
    var xa = parseInt( el2.offset().left );
    var ya = parseInt( el2.offset().top );
    var xb = parseInt( el2.outerWidth() + xa );
    var yb = parseInt( el2.outerHeight() + ya );
    
    // TODO quick check id empty intersection
    if( x0 > xb
        || x1 < xa
        || y0 > yb
        || y1 < ya )
        return 0;
    
    // intersection points
    var px0 = Math.max(x0, xa);
    var px1 = Math.min(x1, xb);
    var py0 = Math.max(y0, ya);
    var py1 = Math.min(y1, yb);
    
    var idx = px1 - px0;
    var idy = py1 - py0;
       
    // intersection area
    var iarea = idx * idy;
    
    var pdx = Math.min(x1 - x0, xb - xa);
    var pdy = Math.min(y1 - y0, yb - ya);
    
    return iarea / (pdx * pdy);
}

/* return relative positioning Pab to Pxy: 0 = after, 1 = before */
function getRelativePositioning(x0, y0, x1, y1, xa, ya, xb, yb) {    
  
    var dxl = xa - x0; // if positive new element starts on the left
    var dxr = x1 - xb; // if positive new element end on the right
    var dyt = ya - y0; // if positive new element starts above the existing one
    var dyb = y1 - yb; // if positive new element starts below the existing one
    
    var relpos = 0;
    var horizontalRelPos = 0;
    var verticalRelPos = 0;
    if( dxl <= 0 && dxr <= 0 ) horizontalRelPos = -1;
    else if( dxl >= dxr ) horizontalRelPos = 1;
    
    if( dyt <= 0 && dyb <= 0 ) verticalRelPos = -1;
    else if( dyt >= dyb ) verticalRelPos = 1;
    
    
    //console.log('H: '+horizontalRelPos+', V: '+verticalRelPos);
    
    if( horizontalRelPos == -1 && verticalRelPos == -1) return 1;
    if( verticalRelPos == -1 ) return horizontalRelPos;
    /*if( horizontalRelPos == -1 )*/ return verticalRelPos;
    return 1;
}
    
/* return relative positioning el1 to el2: 0 = after, 1 = before, -1 unknown */
function getRelativePositioningEl(el1, el2) {
    var x0 = parseInt( el1.offset().left );
    var y0 = parseInt( el1.offset().top );
    var x1 = parseInt( el1.outerWidth() + x0 );
    var y1 = parseInt( el1.outerHeight() + y0 );
    
    var xa = parseInt( el2.offset().left );
    var ya = parseInt( el2.offset().top );
    var xb = parseInt( el2.outerWidth() + xa );
    var yb = parseInt( el2.outerHeight() + ya );
    
    var dxl = xa - x0; // if positive new element starts on the left
    var dxr = x1 - xb; // if positive new element end on the right
    var dyt = ya - y0; // if positive new element starts above the existing one
    var dyb = y1 - yb; // if positive new element starts below the existing one
    		
    var relpos = 0;
    var horizontalRelPos = -1;
    var verticalRelPos = -1;
    
    if( dxl <= 0 && dxr <= 0 ) horizontalRelPos = -1;
    else if( dxl > 0 && dxr > 0 && ( Math.max(dxl, dxr) / Math.min(dxl, dxr) > 2 ) ) {
        dHalfLeft = dxl + (x1 - x0) / 2;
        dHalfRight = dxr + (x1 - x0) / 2;
        
        if( dHalfLeft / dHalfRight > 1.6 ) horizontalRelPos = 1;
        else if(dHalfRight / dHalfLeft > 1.6 ) horizontalRelPos = 0;
        
        //if( dxl > dxr ) horizontalRelPos = 1;
        //else if( dxl < dxr ) horizontalRelPos = 0;
    }
    else if( dxl > dxr /*&& (x1 - x0) / dxl < 2*/ ) horizontalRelPos = 1;
    else if( dxr > dxl /*&& (x1 - x0) / dxr < 2*/ ) horizontalRelPos = 0;
    
    if( dyt <= 0 && dyb <= 0 ) verticalRelPos = -1;
    else if( dyt > 0 && dyb > 0 && ( Math.max(dyt, dyb) / Math.min(dyt, dyb) > 2 ) ) {
        var dHalfTop = dyt + (y1 - y0) / 2;
        var dHalfBot = dyb + (y1 - y0) / 2;
          
        if( dHalfTop / dHalfBot > 1.6 ) verticalRelPos = 1;
        else if ( dHalfBot / dHalfTop > 1.6 ) verticalRelPos = 0;
        //if( dyt > dyb ) verticalRelPos = 1;
        //else if( dyb > dyt ) verticalRelPos = 0;
    }
    else if( dyt > dyb /*&& (y1 - y0) / dyt < 2*/ ) verticalRelPos = 1;
    else if( dyb > dyt /*&& (y1 - y0) / dyb < 2*/ ) verticalRelPos = 0;
    
    
    //console.log('H ('+dxl+','+dxr+'): '+horizontalRelPos+', V ('+dyt+','+dyb+'): '+verticalRelPos);
    
    if( horizontalRelPos == -1 && verticalRelPos == -1) return -1;
    if( verticalRelPos == -1 ) return horizontalRelPos;
    /*if( horizontalRelPos == -1 )*/ return verticalRelPos;
    return -1;
}


}(jqbitDD));